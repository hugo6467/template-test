import { defineStore } from 'pinia'

interface IGeneralState {
  name: string
}
export const useGeneral = defineStore('general', {
  state: ():IGeneralState => {
    return {
      name: ''
    }
  }
})
