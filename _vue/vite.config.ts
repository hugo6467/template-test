import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Unocss from '@unocss/vite'
import { presetAttributify, presetUno, presetIcons } from 'unocss'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import viteSharedConfig from '../shared/vite.shared.config'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(),
    Unocss({
      presets: [
        presetAttributify(),
        presetUno(),
        presetIcons()
      ],
      ...viteSharedConfig.Unocss
    })
  ],
  resolve: {
    alias: {
      ...viteSharedConfig.resolve.alias(path,'vue')
    }
  }
})
