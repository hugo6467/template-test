export default {
  Unocss: {
      shortcuts: {
        btn: 'm-2.5 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded',
      }
    },
    resolve: {
        alias: (path:any, framework:string) => {
            return {
                '@': path.resolve(__dirname, `../_${framework}/src`),
                shared: path.resolve(__dirname, './'),
                core: path.resolve(__dirname, '../core')
            }
        }
    }
}
